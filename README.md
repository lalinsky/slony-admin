Slony-I administration tool
===========================

This is a very simple script created out of frustration with not being able to
remember how to basic maintenance of my Slony-I replication cluster.

Normally, you are expected to either know how write [slonik scripts][slonik] or use
the [`slonik_xxx` administration scripts][altscripts], which are rather low-level and
require additional configuration. For example, in order to add a table
to an existing set you have to know that you need to create a new set,
add the table to the new set, subscribe the nodes to this set, sync it
and then merge the new set into the existing one.

The idea here is to have a high-level administration tool, that knows how
to perfom the most common tasks and generates slonik scripts to do them.
It reads the same configuration file that you already have for the slon
process, so there is no need for additional configuration.

It only supports operations that I needed so far, so it's very far from complete.
Feel free to add new commands and send me pull requests with the changes.
Thanks!

## Configuration

In this guide I will assume that you are running `slony_admin.py` on one of the nodes
which are running [`slon`][slon] and that you are using the Debian/Ubuntu way of
configuring `slon`.

You can either export an environment variable that points to the `slon` configuration
for your cluster:

    export SLONY_ADMIN_CONFIG=/etc/slony1/my_cluster/slon.conf

Or you can use the `-c` parameter each time you run `slony_admin.py`:

    ./slony_admin.py -c /etc/slony1/my_cluster/slon.conf ...

## Getting info about the cluster

Show all nodes in the cluster:

    ./slony_admin list_nodes

Show all sets, including their tables, from the cluster:

    ./slony_admin list_sets

## Adding a new table to an existing set

Let's say we want to add table `my_table` to set `1`. We can do that using a command like this:

    ./slony_admin.py add_table 1 public.my_table | slonik

Note that we are calling the table `publc.my_table`, not just `my_table`. Slony prefers fully
qualified table names.

Behind the scenes, the above command creates a new temporary set with that one table, waits until
the table is replicated and then merges the temporary set into set `1`.

You can also add multiple tables at the same time:

    ./slony_admin.py add_table 1 public.my_table public.my_table_2 | slonik

## Removing a replicated table

This command will stop replicating table `my_table` from set `1`:

    ./slony_admin.py drop_table 1 public.my_table | slonik

After this finishes, you can actually drop the table from the database(s) if you don't need it anymore.

## Adding a column to an existing replicated table

Create an SQL script with the necessary DDL commands:

    ALTER TABLE my_table ADD COLUMN my_new_column VARCHAR;

In this example, we are modifying `my_table` which is from set `1`.

The script can be executed like this:

    ./slony_admin.py execute_script 1 add_my_new_column.sql | slonik

[slonik]: http://slony.info/documentation/2.0/slonikref.html
[altscripts]: http://slony.info/documentation/2.0/adminscripts.html
[slon]: http://slony.info/documentation/2.0/slon.html
