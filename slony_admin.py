#!/usr/bin/env python

import os
import re
import psycopg2
from argparse import ArgumentParser


def connect_db(conn_info):
    conn = psycopg2.connect(conn_info)

    cursor = conn.cursor()
    cursor.execute("SET search_path TO _%s" % (cluster_name,))

    return conn


def ensure_fqn(name):
    if "." not in name:
        return "public." + name
    return name


def parse_fqn(name):
    if "." not in name:
        return "public", name
    return name.split(".")


def parse_config(path):
    config = {}
    if not path:
        return config
    for line in open(path, 'r'):
        line = line.strip()
        if line.startswith('#') or not line:
            continue
        args = line.split(None, 1)
        if len(args) == 2:
            name, value = args
            if value[0] == "'" and value[-1] == "'":
                value = value[1:-1]
            config[name] = value
    return config


def fetch_scalar(sql, *args, **kwargs):
    local_conn = kwargs.pop('conn', conn)
    cursor = local_conn.cursor()
    cursor.execute(sql, *args)
    row = cursor.fetchone()
    return row[0] if row is not None else None


def fetch_list(sql, *args, **kwargs):
    local_conn = kwargs.pop('conn', conn)
    cursor = local_conn.cursor()
    cursor.execute(sql, *args)
    return cursor.fetchall()


def slonik_echo(message):
    return "echo ' => %s';" % message


def slonik_command(command_name, **args):
    command_params = []
    for name, value in args.items():
        name = name.replace('_', ' ')
        value = str(value)
        if not re.match('^(@[a-zA-Z0-9_]+|[0-9]+|YES|NO|ALL)$', value, re.I):
            value = "'%s'" % value
        command_params.append('%s = %s' % (name, value))
    return '%s ( %s );' % (command_name, ', '.join(command_params))


info = {}
override_conn_info = {}


def gather_info():
    info = {'nodes': [], 'paths': [], 'conn_info': {}}
    local_node_id = info['local_node_id'] = fetch_scalar("SELECT last_value FROM sl_local_node_id")

    nodes = fetch_list("SELECT no_id FROM sl_node")
    for node_id, in nodes:
        info['nodes'].append(node_id)

    nodes = fetch_list("SELECT pa_server, pa_conninfo FROM sl_path WHERE pa_client=%s", (local_node_id,))
    for node_id, node_conn_info in nodes:
        if node_conn_info == 'Event pending':
            continue
        info['paths'].append((local_node_id, node_id, node_conn_info))

    for remote_node_id, remote_node_conn_info in nodes:
        if remote_node_conn_info == 'Event pending':
            continue
        remote_node_conn = connect_db(remote_node_conn_info)
        remote_nodes = fetch_list("SELECT pa_server, pa_conninfo FROM sl_path WHERE pa_client=%s", (remote_node_id,), conn=remote_node_conn)
        for node_id, node_conn_info in remote_nodes:
            if node_conn_info == 'Event pending':
                continue
            info['paths'].append((remote_node_id, node_id, node_conn_info))

    for client_node_id, server_node_id, node_conn_info in info['paths']:
        if server_node_id not in info['conn_info']:
            info['conn_info'][server_node_id] = node_conn_info
        elif info['conn_info'][server_node_id] != node_conn_info:
            raise Exception('duplicate conninfo for node %s')

    return info


def preamble():
    yield "define cluster %s;" % cluster_name
    yield "define conn_local '%s';" % conn_info
    for node_id in sorted(info['nodes']):
        node_conn_info = info['conn_info'].get(node_id)
        node_conn_info = override_conn_info.get(node_id, node_conn_info)
        if node_conn_info:
            yield "define conn_%d '%s';" % (node_id, node_conn_info)
    yield ""

    yield "cluster name = @cluster;"
    for node_id in sorted(info['nodes']):
        if node_id == info['local_node_id']:
            yield "node %s admin conninfo = @conn_local;" % (node_id, )
        else:
            yield "node %s admin conninfo = @conn_%d;" % (node_id, node_id)
    yield ""


def merge_set(target_set_id, source_set_id, origin_id=None):
    if origin_id is None:
        origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (target_set_id, ))
    yield "merge set ( id = %s, add id = %s, origin = %s );" % (target_set_id, source_set_id, origin_id)
    yield "wait for event ( origin = %s, confirmed = all, wait on = %s );" % (origin_id, origin_id)


def subscribe_set(set_id, provider_id, receiver_id, origin_id=None, forward=False, omit_copy=False):
    if origin_id is None:
        origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (set_id, ))
    args = dict(id=set_id, provider=provider_id, receiver=receiver_id)
    if forward:
        args['forward'] = 'YES'
    if omit_copy:
        args['omit_copy'] = 'YES'
    yield slonik_command('subscribe set', **args)
    yield slonik_command('sync', id=origin_id)
    yield slonik_command('wait for event', origin=origin_id, confirmed='all', wait_on=origin_id)


def unsubscribe_set(set_id, receiver_id, origin_id=None):
    if origin_id is None:
        origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (set_id, ))
    yield "unsubscribe set ( id = %s, receiver = %s );" % (set_id, receiver_id)
    yield "sync ( id = %s );" % (origin_id, )
    yield "wait for event ( origin = %s, confirmed = all, wait on = %s );" % (origin_id, origin_id)


def create_set(origin_id, comment):
    set_id = fetch_scalar("SELECT max(set_id) + 1 FROM sl_set")
    yield slonik_command("create set", id=set_id, origin=origin_id, comment=comment)
    yield slonik_echo('set %s created' % (set_id,))


def drop_set(set_id):
    origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (set_id, ))
    yield slonik_command("drop set", id=set_id, origin=origin_id)
    yield slonik_echo('set %s dropped' % (set_id,))


def add_table(set_id, *table_names):
    tmp_set_id = fetch_scalar("SELECT max(set_id) + 1 FROM sl_set")
    origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (set_id, ))
    table_id = fetch_scalar("SELECT max(tab_id) + 1 FROM sl_table")
    yield "create set ( id = %s, origin = %s, comment = 'tmp set' );" % (tmp_set_id, origin_id)
    for table_name in table_names:
        table_name = ensure_fqn(table_name)
        yield "set add table ( set id = %s, origin = %s, id = %s, fully qualified name = '%s', comment = '%s' );" % (tmp_set_id, origin_id, table_id, table_name, table_name)
        table_id += 1
    subs = fetch_list("SELECT sub_provider, sub_receiver FROM sl_subscribe WHERE sub_set = %s", (set_id, ))
    for provider_id, receiver_id in subs:
        for command in subscribe_set(tmp_set_id, provider_id, receiver_id, origin_id):
            yield command
    for command in merge_set(set_id, tmp_set_id, origin_id):
        yield command


def drop_table(set_id, *table_names):
    origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (set_id, ))
    for table_name in table_names:
        schema, name = parse_fqn(table_name)
        table_id = fetch_scalar("SELECT tab_id FROM sl_table WHERE tab_set = %s AND tab_nspname = %s AND tab_relname = %s", (set_id, schema, name))
        if table_id is None:
            raise Exception('could not find table %s in set %s' % (table_name, set_id))
        yield "set drop table ( origin = %s, id = %s );" % (origin_id, table_id)
        yield "wait for event ( origin = %s, confirmed = all, wait on = %s );" % (origin_id, origin_id)


def add_sequence(set_id, *sequence_names):
    tmp_set_id = fetch_scalar("SELECT max(set_id) + 1 FROM sl_set")
    origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (set_id, ))
    sequence_id = fetch_scalar("SELECT max(seq_id) + 1 FROM sl_sequence")
    yield "create set ( id = %s, origin = %s, comment = 'tmp set' );" % (tmp_set_id, origin_id)
    for sequence_name in sequence_names:
        sequence_name = ensure_fqn(sequence_name)
        yield "set add sequence ( set id = %s, origin = %s, id = %s, fully qualified name = '%s', comment = '%s' );" % (tmp_set_id, origin_id, sequence_id, sequence_name, sequence_name)
        sequence_id += 1
    subs = fetch_list("SELECT sub_provider, sub_receiver FROM sl_subscribe WHERE sub_set = %s", (set_id, ))
    for provider_id, receiver_id in subs:
        for command in subscribe_set(tmp_set_id, provider_id, receiver_id, origin_id):
            yield command
    for command in merge_set(set_id, tmp_set_id, origin_id):
        yield command


def drop_sequence(set_id, *sequence_names):
    origin_id = fetch_scalar("SELECT set_origin FROM sl_set WHERE set_id = %s", (set_id, ))
    for sequence_name in sequence_names:
        schema, name = parse_fqn(sequence_name)
        sequence_id = fetch_scalar("SELECT seq_id FROM sl_sequence WHERE seq_set = %s AND seq_nspname = %s AND seq_relname = %s", (set_id, schema, name))
        if sequence_id is None:
            raise Exception('could not find sequence %s in set %s' % (sequence_name, set_id))
        yield "set drop sequence ( origin = %s, id = %s );" % (origin_id, sequence_id)
        yield "wait for event ( origin = %s, confirmed = all, wait on = %s );" % (origin_id, origin_id)


def drop_node(node_id):
    local_node_id = fetch_scalar("SELECT last_value FROM sl_local_node_id")
    yield "drop node ( id = %s, event node = %s );" % (node_id, local_node_id)
    yield "wait for event ( origin = %s, confirmed = %s, wait on = %s );" % (local_node_id, local_node_id, local_node_id)


def store_node(conn_info, comment):
    node_id = fetch_scalar("SELECT max(no_id) + 1 FROM sl_node")
    local_node_id = fetch_scalar("SELECT last_value FROM sl_local_node_id")
    yield "node %s admin conninfo = '%s';" % (node_id, conn_info)
    yield "store node ( id = %s, comment = '%s', event node = %s );" % (node_id, comment, local_node_id)
    yield "wait for event ( origin = %s, confirmed = all, wait on = %s );" % (local_node_id, local_node_id)


def store_path(client_node_id, server_node_id, conn_info):
    local_node_id = fetch_scalar("SELECT last_value FROM sl_local_node_id")
    yield "store path ( client = %s, server = %s, conninfo = '%s' );" % (client_node_id, server_node_id, conn_info)
    yield "wait for event ( origin = %s, confirmed = all, wait on = %s );" % (local_node_id, local_node_id)


def update_functions(*node_ids):
    if not node_ids:
        rows = fetch_list("SELECT no_id FROM sl_node ORDER BY no_id")
        node_ids = [r[0] for r in rows]
    for node_id in node_ids:
        yield "update functions ( id = %s );" % (node_id,)


def failover(node_id, backup_node_id):
    yield slonik_command('failover', id=node_id, backup_node=backup_node_id)


def clone_prepare(node_id, provider_node_id, comment):
    yield slonik_echo('preparing cloning node %s from node %s' % (node_id, provider_node_id))
    yield slonik_command('clone prepare', id=node_id, provider=provider_node_id, comment=comment)

    yield ''
    yield '# wait until all nodes have received the event'
    yield slonik_command('wait for event', origin=provider_node_id, confirmed='ALL', wait_on=provider_node_id)

    master_node_id = fetch_scalar("SELECT DISTINCT sub_provider FROM sl_subscribe WHERE sub_receiver = %s", (provider_node_id, ))

    yield ''
    yield '# create a SYNC event and wait on every other node until they have confirmed'
    yield slonik_command('sync', id=master_node_id)
    yield slonik_command('wait for event', origin=master_node_id, confirmed=provider_node_id, wait_on=provider_node_id)

    yield ''
    yield slonik_echo('clone prepared')
    yield slonik_echo('now copy the database from node %s to node %s and then run `clone_finish`' % (provider_node_id, node_id))


def clone_finish(node_id, provider_node_id, node_conn_info):
    override_conn_info[node_id] = node_conn_info
    yield slonik_echo('trying to finish cloning node %s from node %s' % (node_id, provider_node_id))
    yield slonik_command('clone finish', id=node_id, provider=provider_node_id)

    yield ""
    yield slonik_echo('storing paths to and from node %s' % (node_id,))
    for other_node_id in info['nodes']:
        if other_node_id == node_id:
            continue
        yield slonik_command('store path', client=node_id, server=other_node_id, conninfo='@conn_%d' % other_node_id)
        yield slonik_command('store path', client=other_node_id, server=node_id, conninfo='@conn_%s' % node_id)

    yield ""
    yield slonik_echo('clone finished')
    yield slonik_echo('now start the slon process on node %s' % (node_id,))


def execute_script(set_id, script):
    yield slonik_command('execute script', set_id=set_id, filename=script, event_node=info['local_node_id'])


def print_commands(commands, indent=0):
    for command in commands:
        print " " * indent + command


def print_script(commands):
    commands = list(commands)
    print_commands(preamble())
    print_commands(commands)


def cmd_preamble(args):
    print_script([])


def cmd_create_set(args):
    commands = create_set(args.provider_id, args.comment)
    print_script(commands)


def cmd_drop_set(args):
    commands = drop_set(args.set_id)
    print_script(commands)


def cmd_add_table(args):
    commands = add_table(args.set_id, *args.table)
    print_script(commands)


def cmd_drop_table(args):
    commands = drop_table(args.set_id, *args.table)
    print_script(commands)


def cmd_add_sequence(args):
    commands = add_sequence(args.set_id, *args.sequence)
    print_script(commands)


def cmd_drop_sequence(args):
    commands = drop_sequence(args.set_id, *args.sequence)
    print_script(commands)


def cmd_merge_set(args):
    commands = merge_set(args.target_set_id, args.source_set_id)
    print_script(commands)


def cmd_subscribe_set(args):
    commands = subscribe_set(args.set_id, args.provider_node_id, args.receiver_node_id, forward=args.forward, omit_copy=args.omit_copy)
    print_script(commands)


def cmd_unsubscribe_set(args):
    commands = unsubscribe_set(args.set_id, args.receiver_node_id)
    print_script(commands)


def cmd_store_node(args):
    commands = store_node(args.node_conn_info, args.comment)
    print_script(commands)


def cmd_drop_node(args):
    commands = drop_node(args.node_id)
    print_script(commands)


def cmd_store_path(args):
    commands = store_path(args.client_node_id, args.server_node_id, args.node_conn_info)
    print_script(commands)


def cmd_update_functions(args):
    commands = update_functions(*args.node_id)
    print_script(commands)


def cmd_failover(args):
    commands = failover(args.node_id, args.backup_node_id)
    print_script(commands)


def cmd_clone_prepare(args):
    commands = clone_prepare(args.node_id, args.provider_node_id, args.comment)
    print_script(commands)


def cmd_clone_finish(args):
    commands = clone_finish(args.node_id, args.provider_node_id, args.node_conn_info)
    print_script(commands)


def cmd_execute_script(args):
    if not os.path.exists(args.script):
        parser.error('script %s does not exist' % args.script)
    commands = execute_script(args.set_id, args.script)
    print_script(commands)


def cmd_list_sets(args):
    rows = fetch_list("SELECT set_id, set_origin, set_locked, set_comment FROM sl_set ORDER BY set_id")
    print "There are %s set(s) in the cluster %s:" % (len(rows), cluster_name)
    for set_id, origin_id, locked, comment in rows:
        print
        print "ID:", set_id
        print "Origin node ID:", origin_id
        print "Locked:", "YES" if locked else "NO"
        print "Comment:", comment.strip()
        print "Tables:"
        for tab_id, tab_name, tab_schema in fetch_list("SELECT tab_id, tab_relname, tab_nspname FROM sl_table WHERE tab_set = %s ORDER BY tab_nspname, tab_relname", (set_id, )):
            print "    %s.%s (%s)" % (tab_schema, tab_name, tab_id)
        print "Sequences:"
        for seq_id, seq_name, seq_schema in fetch_list("SELECT seq_id, seq_relname, seq_nspname FROM sl_sequence WHERE seq_set = %s ORDER BY seq_nspname, seq_relname", (set_id, )):
            print "    %s.%s (%s)" % (seq_schema, seq_name, seq_id)
        print "Subscribed nodes:"
        for provider_id, receiver_id, forward, active in fetch_list("SELECT sub_provider, sub_receiver, sub_forward, sub_active FROM sl_subscribe WHERE sub_set = %s ORDER BY sub_provider, sub_receiver", (set_id, )):
            print "    node %s => node %s%s%s" % (provider_id, receiver_id, " (not active)(" if not active else "", " (forward)" if forward else "")


def cmd_list_nodes(args):
    rows = fetch_list("SELECT no_id, no_active, no_comment FROM sl_node ORDER BY no_id")
    print "There are %s node(s) in the cluster %s:" % (len(rows), cluster_name)
    for node_id, active, comment in rows:
        print
        print "ID:", node_id
        print "Active:", "YES" if active else "NO"
        print "Comment:", comment.strip()
        print "Connections:"
        for server_id, server_conn_info in fetch_list("SELECT pa_server, pa_conninfo FROM sl_path WHERE pa_client = %s ORDER BY pa_server", (node_id, )):
	    print "    node %s via '%s'" % (server_id, server_conn_info)
        print "Subscribed sets:"
        for set_id, forward, active in fetch_list("SELECT DISTINCT sub_set, sub_forward, sub_active FROM sl_subscribe WHERE sub_provider = %s ORDER BY sub_set", (node_id, )):
            print "    set %s (master)%s%s" % (set_id, " (not active)" if not active else "", " (forward)" if forward else "")
        for set_id, forward, active in fetch_list("SELECT DISTINCT sub_set, sub_forward, sub_active FROM sl_subscribe WHERE sub_receiver = %s ORDER BY sub_set", (node_id, )):
            print "    set %s (slave)%s%s" % (set_id, " (not active)" if not active else "", " (forward)" if forward else "")


global_help='''
This script has two main use cases, displaying information about a Slony-I
replication cluster and generating slonik scripts for making changes to the
cluster. Commands that have (slonik) in the description produce output 
suitable for piping into the slonik tool.
'''

parser = ArgumentParser(description=global_help)
parser.add_argument('-c', '--config', help='slon configuration file')
parser.add_argument('-r', '--cluster', dest='cluster_name', help='slony cluster name')
parser.add_argument('-n', '--conn', dest='conn_info', help='connection string to the slony cluster')

subparsers = parser.add_subparsers()

p = subparsers.add_parser('preamble', help='print the premable for custom scripts (slonik)')
p.set_defaults(func=cmd_preamble)

p = subparsers.add_parser('create_set', help='add a new set (slonik)')
p.add_argument('provider_id', type=int, help='ID of the provider node')
p.add_argument('comment', help='set comment')
p.set_defaults(func=cmd_create_set)

p = subparsers.add_parser('drop_set', help='drop a set (slonik)')
p.add_argument('set_id', type=int, help='ID of the set to drop')
p.set_defaults(func=cmd_drop_set)

p = subparsers.add_parser('add_table', help='add new table(s) to an existing set (slonik)')
p.add_argument('set_id', type=int, help='ID of the set to which the table(s) should be added')
p.add_argument('table', nargs='+', help='fully qualified names of the tables to add')
p.set_defaults(func=cmd_add_table)

p = subparsers.add_parser('drop_table', help='remove a table from a set (slonik)')
p.add_argument('set_id', type=int, help='ID of the set from which the table(s) should be removed')
p.add_argument('table', nargs='+', help='fully qualified names of the tables to remove')
p.set_defaults(func=cmd_drop_table)

p = subparsers.add_parser('add_sequence', help='add new sequence(s) to an existing set (slonik)')
p.add_argument('set_id', type=int, help='ID of the set to which the sequence(s) should be added')
p.add_argument('sequence', nargs='+', help='fully qualified names of the sequences to add')
p.set_defaults(func=cmd_add_sequence)

p = subparsers.add_parser('drop_sequence', help='remove a sequence from a set (slonik)')
p.add_argument('set_id', type=int, help='ID of the set from which the sequence(s) should be removed')
p.add_argument('sequence', nargs='+', help='fully qualified names of the sequences to remove')
p.set_defaults(func=cmd_drop_sequence)

p = subparsers.add_parser('merge_set', help='merge two replication sets together (slonik)')
p.add_argument('target_set_id', type=int, help='ID of the node that will contain the merged node')
p.add_argument('source_set_id', type=int, help='ID of the node to merge')
p.set_defaults(func=cmd_merge_set)

p = subparsers.add_parser('subscribe_set', help='start replication of a set (slonik)')
p.add_argument('set_id', type=int, help='ID of the set to subscribe')
p.add_argument('provider_node_id', type=int, help='ID of the source node')
p.add_argument('receiver_node_id', type=int, help='ID of the target node')
p.add_argument('--forward', action='store_true')
p.add_argument('--omit-copy', action='store_true')
p.set_defaults(func=cmd_subscribe_set)

p = subparsers.add_parser('unsubscribe_set', help='stop replication of a set (slonik)')
p.add_argument('set_id', type=int, help='ID of the set to unsubscribe')
p.add_argument('receiver_node_id', type=int, help='ID of the target node')
p.set_defaults(func=cmd_unsubscribe_set)

p = subparsers.add_parser('list_sets', help='list all sets in the cluster')
p.set_defaults(func=cmd_list_sets)

p = subparsers.add_parser('list_nodes', help='list all nodes in the cluster')
p.set_defaults(func=cmd_list_nodes)

p = subparsers.add_parser('store_node', help='add new replication node (slonik)')
p.add_argument('node_conn_info', help='connection string')
p.add_argument('comment', default='a node', help='comment')
p.set_defaults(func=cmd_store_node)

p = subparsers.add_parser('drop_node', help='remove a replication node (slonik)')
p.add_argument('node_id', help='ID of the node to drop')
p.set_defaults(func=cmd_drop_node)

p = subparsers.add_parser('store_path', help='add new replication node (slonik)')
p.add_argument('client_node_id', type=int, help='ID of the client node')
p.add_argument('server_node_id', type=int, help='ID of the server node')
p.add_argument('node_conn_info', help='connection string')
p.set_defaults(func=cmd_store_path)

p = subparsers.add_parser('update_functions', help='update functions to a new minor version (slonik)')
p.add_argument('node_id', nargs='*', type=int, help='ID of the node to update')
p.set_defaults(func=cmd_update_functions)

p = subparsers.add_parser('failover', help='fail a broken replication set over to a backup node (slonik)')
p.add_argument('node_id', type=int, help='ID of the failed node')
p.add_argument('backup_node_id', type=int, help='ID of the node that will take over all sets originating on the failed node')
p.set_defaults(func=cmd_failover)

p = subparsers.add_parser('clone_prepare', help='prepare cloning a node (slonik)')
p.add_argument('node_id', type=int, help='ID of the new cloned node')
p.add_argument('provider_node_id', type=int, help='ID of the node that the new node was cloned from')
p.add_argument('comment', help='node comment')
p.set_defaults(func=cmd_clone_prepare)

p = subparsers.add_parser('clone_finish', help='complete cloning a node (slonik)')
p.add_argument('node_id', type=int, help='ID of the new cloned node')
p.add_argument('provider_node_id', type=int, help='ID of the node that the new node was cloned from')
p.add_argument('node_conn_info', help='connection string for the new node')
p.set_defaults(func=cmd_clone_finish)

execute_script_help = '''\
Execute an SQL DDL script on all nodes of the cluster at the same time.
This is needed if you want to change a replicated table, e.g. by adding a new column.
http://slony.info/documentation/2.0/stmtddlscript.html
'''

p = subparsers.add_parser('execute_script', help='execute a DDL script on the cluster (slonik)', description=execute_script_help)
p.add_argument('set_id', type=int, help='ID of the replicated set which the scripts affects')
p.add_argument('script', help='path to the SQL script to execute')
p.set_defaults(func=cmd_execute_script)

args = parser.parse_args()

config = parse_config(args.config or os.environ.get('SLONY_ADMIN_CONFIG'))

cluster_name = args.cluster_name or config.get('cluster_name')
if not cluster_name:
    parser.error('missing cluster name')

conn_info = args.conn_info or config.get('conn_info')
if not conn_info:
    parser.error('missing connection info')

conn = connect_db(conn_info)
info = gather_info()

args.func(args)

